const Joi = require('joi')

const schema = Joi.object({
  Book_id: Joi.string().required().uuid(),
  Book_title: Joi.string().required(),
  Book_author: Joi.string().required(),
  Book_price: Joi.number().strict().required()
})

module.exports = schema
