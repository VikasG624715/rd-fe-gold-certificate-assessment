const { Router } = require('express')
const { getAllBooks, createBook, getBookByID, updateBookById, deleteBookById } = require('../controllers/books.controller')

const router = Router()

router.route('/')
  .get(getAllBooks)
  .post(createBook)
router.route('/:BookId')
  .get(getBookByID)
  .put(updateBookById)
  .delete(deleteBookById)

module.exports = router
