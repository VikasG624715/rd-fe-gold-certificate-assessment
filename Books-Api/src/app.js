const express = require('express')
const bookRoutes = require('../src/routes/books.routes')
const { errorHandler } = require('../src/middlewares/errorMiddleware')

require('dotenv').config()
const app = express()

const PORT = process.env.PORT || 5000

app.use(express.json())
app.use(errorHandler)

app.use('/BooksApi/v1/books', bookRoutes)

module.exports = app

if (require.main === module) {
  app.listen(PORT, () => {
    console.log(`app is listening at port: ${PORT}`)
  })
}
