const { errorResponse } = require('../utils/responseUtils')

const errorHandler = (err, req, res, next) => {
  return errorResponse(res, 500, err.message || 'Internal Server Error')
}

module.exports = {
  errorHandler
}
