const { generateBookId } = require('../utils/bookUtils')
const { validateBook } = require('../services/booksValidation')
const { sortBooks, searchByAuthor, searchByPrice, searchByTitle, paginateBooks } = require('../services/queryService')
const { readDataFromFile, writeDataToFile } = require('../utils/fileUtils')

const getAllBooksService = (queryParams) => {
  const books = readDataFromFile()
  let returnBooks = [...books]
  if (queryParams) {
    const { limit, sort, title, author, price, page } = queryParams
    if (author) {
      returnBooks = [...searchByAuthor(returnBooks, author)]
    }
    if (title) {
      returnBooks = [...searchByTitle(returnBooks, title)]
    }
    if (price) {
      returnBooks = [...searchByPrice(returnBooks, price)]
    }
    if (sort) {
      returnBooks = [...sortBooks(returnBooks, sort)]
    }
    if (page) {
      returnBooks = [...paginateBooks(returnBooks, page, limit)]
    }
  }
  return returnBooks
}

const createBookService = (newBookDetails) => {
  const books = readDataFromFile()
  const newBook = {
    ...generateBookId(),
    ...newBookDetails
  }
  const { error } = validateBook(newBook)
  if (!error) {
    books.push(newBook)
    writeDataToFile(books)
    return {
      newBook,
      error: null
    }
  } else {
    return {
      newBook: null,
      error
    }
  }
}

const getBookByIDService = (requestedBookID) => {
  const books = readDataFromFile()
  const book = books.find(book => book.Book_id === requestedBookID)
  return book
}
const updateBookByIdService = (requestedBookID, updatedBookDetails, res) => {
  const books = readDataFromFile()
  const bookIdx = books.findIndex(book => book.Book_id === requestedBookID)
  if (bookIdx !== -1) {
    const updatedBook = {
      ...books[bookIdx],
      ...updatedBookDetails
    }
    const { error } = validateBook(updatedBook)
    if (!error) {
      books[bookIdx] = updatedBook
      writeDataToFile(books)
      return {
        updatedBook,
        error: null
      }
    } else {
      return {
        updatedBook,
        error
      }
    }
  } else {
    return {
      updatedBook: null,
      error: `Book with id ${requestedBookID} not found`
    }
  }
}

const deleteBookByIdService = (requestedBookID) => {
  const books = readDataFromFile()
  const bookIdx = books.findIndex(book => book.Book_id === requestedBookID)
  if (bookIdx !== -1) {
    books.splice(bookIdx, 1)
    writeDataToFile(books)
    return true
  } else {
    return false
  }
}

module.exports = {
  getAllBooksService,
  createBookService,
  getBookByIDService,
  updateBookByIdService,
  deleteBookByIdService
}
