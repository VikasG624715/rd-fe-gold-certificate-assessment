const bookSchema = require('../models/books.model')

const validateBook = (book) => {
  return bookSchema.validate(book)
}

module.exports = { validateBook }
