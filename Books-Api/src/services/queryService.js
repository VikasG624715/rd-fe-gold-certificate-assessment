const sortBooks = (books, key = null) => {
  if (key) {
    books.sort((a, b) => {
      return a[key] - b[key]
    })
    return books
  } else {
    return books
  }
}

const searchByAuthor = (books, author) => {
  return books.filter(book => book.Book_author === author)
}

const searchByTitle = (books, title) => {
  return books.filter(book => book.Book_title === title)
}

const searchByPrice = (books, price) => {
  return books.filter(book => book.Book_price === parseInt(price))
}

const paginateBooks = (books, pageNo, limit = 2) => {
  const idx = limit * (pageNo - 1)
  return books.slice(idx, idx + limit)
}

module.exports = {
  sortBooks,
  searchByAuthor,
  searchByPrice,
  searchByTitle,
  paginateBooks
}
