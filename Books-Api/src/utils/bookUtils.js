const { v4 } = require('uuid')

const generateBookId = () => {
  return {
    Book_id: v4()
  }
}

module.exports = {
  generateBookId
}
