const fs = require('fs')
const path = require('path')

const filepath = path.resolve(__dirname, '../data/booksMock.json')

function readDataFromFile () {
  const data = fs.readFileSync(filepath)
  return JSON.parse(data)
}

function writeDataToFile (data) {
  const jsonData = JSON.stringify(data, null, 2)
  fs.writeFileSync(filepath, jsonData)
}

module.exports = {
  readDataFromFile,
  writeDataToFile
}
