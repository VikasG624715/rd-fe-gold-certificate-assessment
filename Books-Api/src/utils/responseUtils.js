const successResponse = (res, statusCode, message, data) => {
  let responseData = {
    success: true,
    message
  }
  data && (responseData = {
    ...responseData,
    data
  })
  return res.status(statusCode).json(responseData)
}

const errorResponse = (res, statusCode, errorMessage) => {
  return res.status(statusCode).json({
    success: false,
    message: errorMessage
  })
}

module.exports = {
  successResponse,
  errorResponse
}
