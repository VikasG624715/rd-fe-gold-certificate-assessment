const { getAllBooksService, createBookService, getBookByIDService, updateBookByIdService, deleteBookByIdService } = require('../services/booksApiService')
const { successResponse, errorResponse } = require('../utils/responseUtils')

const getAllBooks = (req, res, next) => {
  try {
    const books = getAllBooksService(req.query)
    return (books.length) ? successResponse(res, 200, 'Books Fetched', books) : errorResponse(res, 404, 'No Books')
  } catch (error) {
    next(error)
  }
}

const createBook = (req, res, next) => {
  try {
    const { newBook, error } = createBookService(req.body)
    return !error ? successResponse(res, 201, 'Book Created', newBook) : errorResponse(res, 400, error.message)
  } catch (error) {
    next(error)
  }
}

const getBookByID = (req, res, next) => {
  try {
    const requestedBookID = req.params.BookId
    const book = getBookByIDService(requestedBookID)
    book ? successResponse(res, 200, `Book with id ${requestedBookID} is fetched`, book) : errorResponse(res, 404, `Book with id ${requestedBookID} not found`)
  } catch (error) {
    next(error)
  }
}

const updateBookById = (req, res, next) => {
  try {
    const requestedBookID = req.params.BookId
    const { updatedBook, error } = updateBookByIdService(req.params.BookId, req.body)
    if (updatedBook) {
      return error ? errorResponse(res, 400, error.message) : successResponse(res, 200, `Book with id ${requestedBookID} is updated`, updatedBook)
    } else {
      return errorResponse(res, 404, `Book with id ${requestedBookID} not found`)
    }
  } catch (error) {
    next(error)
  }
}

const deleteBookById = (req, res, next) => {
  try {
    const requestedBookID = req.params.BookId
    deleteBookByIdService(requestedBookID) ? successResponse(res, 200, `Book with id ${requestedBookID} is deleted`) : errorResponse(res, 404, `Book with id ${requestedBookID} not found`)
  } catch (error) {
    next(error)
  }
}

module.exports = {
  getAllBooks,
  getBookByID,
  createBook,
  updateBookById,
  deleteBookById
}
