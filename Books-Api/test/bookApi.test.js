const chai = require('chai')
const expect = chai.expect
const supertest = require('supertest')
const { describe, it, beforeEach } = require('mocha')

const { writeDataToFile } = require('../src/utils/fileUtils')

const app = require('../src/app')

const request = supertest(app)

chai.use(require('chai-sorted'))

describe('Books APi', () => {
  describe('POST /BooksApi/v1/books:BookId', () => {
    beforeEach('clearing persistant store', () => {
      writeDataToFile([])
    })
    it('should add user with valid data', async () => {
      const res = await request.post('/BooksApi/v1/books').send({
        Book_title: 'title1',
        Book_author: 'author1',
        Book_price: 400
      })

      expect(res.status).to.equal(201)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book Created')
      expect(res.body).to.have.property('data')
      expect(res.body.data).to.have.property('Book_id')
      expect(res.body.data.Book_title).to.equal('title1')
      expect(res.body.data.Book_author).to.equal('author1')
      expect(res.body.data.Book_price).to.equal(400)
    })

    it('should should show error for invalid data', async () => {
      const res = await request.post('/BooksApi/v1/books').send({
        Book_title: 1234,
        Book_author: 'author1',
        Book_price: '400'
      })
      expect(res.status).to.equal(400)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(false)
      expect(res.body).to.have.property('message')
    })
  })

  describe('Get /BooksApi/v1/books', () => {
    beforeEach('adding dummy data into persistant store', async () => {
      await writeDataToFile([
        {
          Book_id: 'df6d0a87-4cf3-4f7a-b1ae-07bffcfff836',
          Book_title: 'title1',
          Book_author: 'author1',
          Book_price: 400
        },
        {
          Book_id: '524df1d6-e915-4d62-b363-6f80e3180bae',
          Book_title: 'title2',
          Book_author: 'author2',
          Book_price: 100
        },
        {
          Book_id: '79ad65e5-b53c-42a0-b132-ddbb5a8b38f2',
          Book_title: 'title3',
          Book_author: 'author1',
          Book_price: 600
        }
      ])
    })

    it('Should get all the book data present in books', async () => {
      const res = await request.get('/BooksApi/v1/books')
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Books Fetched')
      expect(res.body).to.have.property('data')
      expect(res.body.data.length).to.equal(3)
    })

    it('should test sorted order of books', async () => {
      const res = await request.get('/BooksApi/v1/books?sort=Book_price')
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Books Fetched')
      expect(res.body).to.have.property('data')
      expect(res.body.data).to.have.sortedBy('Book_price')
    })

    describe('Books Api Pagination', () => {
      it('should get books by pagination with default limit', async () => {
        const res = await request.get('/BooksApi/v1/books?page=1')
        expect(res.status).to.equal(200)
        expect(res.body).to.have.property('success')
        expect(res.body.success).to.equal(true)
        expect(res.body).to.have.property('message')
        expect(res.body.message).to.equal('Books Fetched')
        expect(res.body).to.have.property('data')
        expect(res.body.data.length).to.equal(2)
      })

      it('should get books by pagination with custom limit 1', async () => {
        const res = await request.get('/BooksApi/v1/books?page=1&limit=1')
        expect(res.status).to.equal(200)
        expect(res.body).to.have.property('success')
        expect(res.body.success).to.equal(true)
        expect(res.body).to.have.property('message')
        expect(res.body.message).to.equal('Books Fetched')
        expect(res.body).to.have.property('data')
        expect(res.body.data.length).to.equal(1)
      })
    })

    describe('Searching and Filtering Books', () => {
      it('should search by book author', async () => {
        const res = await request.get('/BooksApi/v1/books?author=author1')
        expect(res.status).to.equal(200)
        expect(res.body).to.have.property('success')
        expect(res.body.success).to.equal(true)
        expect(res.body).to.have.property('message')
        expect(res.body.message).to.equal('Books Fetched')
        expect(res.body).to.have.property('data')
        expect(res.body.data.length).to.equal(2)
      })

      it('should search by book price', async () => {
        const res = await request.get('/BooksApi/v1/books?price=600')
        expect(res.status).to.equal(200)
        expect(res.body).to.have.property('success')
        expect(res.body.success).to.equal(true)
        expect(res.body).to.have.property('message')
        expect(res.body.message).to.equal('Books Fetched')
        expect(res.body).to.have.property('data')
        expect(res.body.data.length).to.equal(1)
      })

      it('should search by book title', async () => {
        const res = await request.get('/BooksApi/v1/books?title=title2')
        expect(res.status).to.equal(200)
        expect(res.body).to.have.property('success')
        expect(res.body.success).to.equal(true)
        expect(res.body).to.have.property('message')
        expect(res.body.message).to.equal('Books Fetched')
        expect(res.body).to.have.property('data')
        expect(res.body.data.length).to.equal(1)
      })
    })

    it('should test combination of pagination and sorting', async () => {
      const res = await request.get('/BooksApi/v1/books?page=1&sort=Book_price')
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Books Fetched')
      expect(res.body).to.have.property('data')
      expect(res.body.data.length).to.equal(2)
      expect(res.body.data).to.have.sortedBy('Book_price')
    })

    it('Should return error message if no books', async () => {
      writeDataToFile([])
      const res = await request.get('/BooksApi/v1/books')
      expect(res.status).to.equal(404)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(false)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('No Books')
    })
  })

  describe('GET /BooksApi/v1/books:BookId', () => {
    beforeEach('adding dummy data into persistant store', async () => {
      await writeDataToFile([{
        Book_id: 'e02f003f-4d98-411b-acd4-eb3cfd71037d',
        Book_title: 'title1',
        Book_author: 'author1',
        Book_price: 400
      }])
    })
    it('should get the book data with valid book id', async () => {
      const res = await request.get('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd71037d')
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book with id e02f003f-4d98-411b-acd4-eb3cfd71037d is fetched')
      expect(res.body).to.have.property('data')
    })

    it('should get error with invalid book id', async () => {
      const res = await request.get('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd710d73')
      expect(res.status).to.equal(404)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(false)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book with id e02f003f-4d98-411b-acd4-eb3cfd710d73 not found')
    })
  })

  describe('PUT /BooksApi/v1/books/:id', () => {
    beforeEach('adding dummy data into persistant store', () => {
      writeDataToFile([{
        Book_id: 'e02f003f-4d98-411b-acd4-eb3cfd71037d',
        Book_title: 'title1',
        Book_author: 'author1',
        Book_price: 400
      }])
    })

    it('Should update book details with valid BookId', async () => {
      const res = await request.put('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd71037d')
        .send({
          Book_price: 500
        })
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book with id e02f003f-4d98-411b-acd4-eb3cfd71037d is updated')
      expect(res.body).to.have.property('data')
      expect(res.body.data.Book_price).to.equal(500)
    })

    it('Should show error when updating book details with invalid BookId', async () => {
      const res = await request.put('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd710d37')
        .send({
          Book_price: 500
        })
      expect(res.status).to.equal(404)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(false)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book with id e02f003f-4d98-411b-acd4-eb3cfd710d37 not found')
    })

    it('Should show error when updating book details with invalid Book Details', async () => {
      const res = await request.put('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd71037d')
        .send({
          Book_price: 'abcd'
        })
      expect(res.status).to.equal(400)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(false)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('"Book_price" must be a number')
    })
  })

  describe('DELETE /BooksApi/v1/books:BookId', () => {
    beforeEach('adding dummy data into persistant store', () => {
      writeDataToFile([{
        Book_id: 'e02f003f-4d98-411b-acd4-eb3cfd71037d',
        Book_title: 'title1',
        Book_author: 'author1',
        Book_price: 400
      }])
    })

    it('Should delete a book with given invalid id', async () => {
      const res = await request.delete('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd710d73')
      expect(res.status).to.equal(404)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(false)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book with id e02f003f-4d98-411b-acd4-eb3cfd710d73 not found')
    })

    it('Should not delete a book with valid id', async () => {
      const res = await request.delete('/BooksApi/v1/books/e02f003f-4d98-411b-acd4-eb3cfd71037d')
      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('success')
      expect(res.body.success).to.equal(true)
      expect(res.body).to.have.property('message')
      expect(res.body.message).to.equal('Book with id e02f003f-4d98-411b-acd4-eb3cfd71037d is deleted')
    })
  })
})
